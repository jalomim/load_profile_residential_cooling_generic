# generic typical day hourly profiles dependent on temperature and country for space cooling in the residential sector




## Repository structure
```
data                    -- containts the dataset in CSV format
readme.md               -- Readme file 
datapackage.json        -- Includes the meta information of the dataset for processing and data integration

```

## Documentation


This dataset provides the hourly heat demand for space cooling in the residential sector for typical days with different temperature levels.  
The profiles can be used to assemble a yearlong demand profile for a NUTS2 region, if the daily average temperature for the specific region and year is available. 

Create your own profile.
For heating and cooling, we provided a yearlong profile for the year 2010. However, we want to give the user the opportunity to use a year of his/her choice. Additionally, if users have access to location-specific hourly temperature profiles, we want to give the user the opportunity to use this data in order to generation load profiles with a higher precision.
Therefore, the generic profiles are supposed to enable the user to produce load profiles of his/her own using own data and a structure year of her/his own choice. 
Structure year in this context means the order of days in the course of the year. In contrast to demand for sanitary hot water, we assume that demand for heating and cooling does not depend on the typeday but only on the hour of the day itself and the outside temperature in the respective hour (for this reason, the column “season” is not relevant for heating and cooling profiles). Technically, due to this temperature-dependency, this profile is also not depending on typedays. However, as the profiles are derived from a profile from Germany and then shifted according to national consumer patterns (see report), we introduced a typeday-dependency.
The column “day type” refers to the type of a day in the week:
- weekdays = typeday 0;
- saturday or day before a holiday = typeday 1;
- sunday or holiday = typeday 2

The profiles provided here are unitless, since they must be scaled during the generation of yearlong profiles. For the generic profiles for heating and cooling, they are driven by the differences between hours and temperature levels.
Yearlong profiles can be generated from the generic profiles provided in this repository following the following steps:
- determining the structure year for which the profiles are generated
- choosing the correct combination of hour of the day, temperature and demand from the generic profile for each hour of the year in order to get a yearlong, unitless profile
- scaling the total sum of the annual yearlong profile (i.e. the integral of the profile) according to the annual total demand


For detailed explanations and a graphical illustration of the dataset please see the [Hotmaps WP2 report](https://www.hotmaps-project.eu/wp-content/uploads/2018/03/D2.3-Hotmaps_for-upload_revised-final_.pdf) section 2.8 page 121ff.


### Limitations of the datasets

The datasets provided have to be interpreted as simplified indicator to enable the analysis that can be carried out within the Hotmaps toolbox. It should be noted that the results of the toolbox can be improved with recorded heat demand data of local representatives of the tertiary sector.


## How to cite

Simon Pezzutto, Stefano Zambotti, Silvia Croce, Pietro Zambelli, Giulia Garegnani, Chiara Scaramuzzino, Ramón Pascual Pascuas, Alyona Zubaryeva, Franziska Haas, Dagmar Exner (EURAC), Andreas Müller (e‐think), Michael Hartner (TUW), Tobias Fleiter, Anna‐Lena Klingler, Matthias Kühnbach, Pia Manz, Simon Marwitz, Matthias Rehfeldt, Jan Steinbach, Eftim Popovski (Fraunhofer ISI) Reviewed by Lukas Kranzl, Sara Fritz (TUW)
Hotmaps Project, D2.3 WP2 Report – Open Data Set for the EU28, 2018 [www.hotmaps-project.eu](https://www.hotmaps-project.eu/wp-content/uploads/2018/03/D2.3-Hotmaps_for-upload_revised-final_.pdf) 


## Authors

Matthias Kühnbach, Simon Marwitz, Anna-Lena Klingler <sup>*</sup>,

<sup>*</sup> [Fraunhofer ISI](https://isi.fraunhofer.de/)
Fraunhofer ISI, Breslauer Str. 48, 
76139 Karlsruhe


## License

Copyright © 2016-2018: Matthias Kuehnbach, Anna-Lena Klingler, Simon Marwitz

Creative Commons Attribution 4.0 International License

This work is licensed under a Creative Commons CC BY 4.0 International License.


SPDX-License-Identifier: CC-BY-4.0

License-Text: https://spdx.org/licenses/CC-BY-4.0.html


## Acknowledgement

We would like to convey our deepest appreciation to the Horizon 2020 [Hotmaps Project](http://www.hotmaps-project.eu/) (Grant Agreement number 723677), which provided the funding to carry out the present investigation.
